import React from 'react';
import './App.css';
import AddRoutine from './Component/AddRoutine';
import CompleteComponent from './Component/CompleteComponent';
import Nav from './Component/Nav';
import ProgressRoutine from './Component/ProgressRoutine';
import TodayRoutine from './Component/TodayRoutine';


function App() {
  const [routine, setNewRoutine] = React.useState([])
  const [isRoutine, setIsRoutine] = React.useState(false)
  const [state,setState] = React.useState([])
  const [complete,setComplete] = React.useState([])
console.log(complete)
  return (
    <>
  
   <Nav  setIsRoutine={setIsRoutine} setState={setState}  setNewRoutine={setNewRoutine} setComplete={setComplete} />
    
    <div>
      {!isRoutine?<AddRoutine setIsRoutine={setIsRoutine} routine={routine} setNewRoutine={setNewRoutine}/>:

      
    <>
    <TodayRoutine routine={routine} setNewRoutine={setNewRoutine} setState={setState} state={state}/>
    <ProgressRoutine  state={state} setState={setState} setComplete={setComplete} complete={complete} />
    <CompleteComponent complete={complete}/>
  </>
      }
    
    </div>
    </>
   
  );
}

export default App;
