import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import Tooltip from '@material-ui/core/Tooltip';
import TextField from '@material-ui/core/TextField';


const useStyles = makeStyles((theme) => ({
    
    margin: {
      marginLeft: theme.spacing(1),
      marginTop:theme.spacing(0)
    },
    extendedIcon: {
      marginRight: theme.spacing(1),
    },
  }));

  

const SingleRoutine = ({routine,setNewRoutine,setError}) => {
    
    const classes = useStyles();
    const [input,setInput] = React.useState('')
    const addingNewRoutine = () => {
        if(input===''){
            setError('Routine should not be empty')
            return
        }
        setNewRoutine([...routine,input])
        
        setError(null)
        setInput('')
       
    }
    const onInputChange = (ev) => {
    
        setInput(ev.target.value)
    
    }
    
    
    return(
        <>
         <p className='type'>{routine.length===0?'Type your Today Routine':null}</p>
           <div className='inputadd'>
               <TextField id="standard-basic" label='Enter Routine' onChange ={onInputChange}  value={input}/>
               <Tooltip title="Add Routine" arrow>
        <IconButton  onClick= {addingNewRoutine} aria-label="Add" className={classes.margin}>
              <AddIcon  fontSize="small" color="secondary" />
            </IconButton>
         </Tooltip>
              
               
           </div>
        </>

    )
}
export default SingleRoutine