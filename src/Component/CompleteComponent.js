//CompleteComponent

import React from "react";
import { Button } from 'rsuite';
import Tooltip from '@material-ui/core/Tooltip';

const CompleteComponent = ({complete}) => {
   

    return(
        <>
        {complete.length===0?null:
        <div className='card3'>
        <div className='todayRoutine'>Completed Routine</div>
        <>
        {complete.map((el,i) => {
            return (
                <Tooltip title="Completed Routine"  key={i} arrow>
                <Button appearance='default' block ><span className="todayRoutine1">{el}</span></Button>
                </Tooltip>
            )
        })}
        </>
    
    </div>
        }
        
        </>
    )
}

export default CompleteComponent