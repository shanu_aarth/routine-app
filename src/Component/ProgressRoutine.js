import React from "react";
import { Button } from 'rsuite';
import Tooltip from '@material-ui/core/Tooltip';

const ProgressRoutine = ({state,setState,setComplete,complete}) => {
    const getProgressRoutine = (el) => {

        setState(state.filter(e => e !==el) )
       setComplete([...complete,el])
 
 
     }

    return(
        <>
        {state.length===0?null:
        <div className='card2'>
        <div className='todayRoutine'>Progress Routine</div>
        <>
        {state.map((el,i) => {
            return (
                <Tooltip title="Add to Complete Routine"  key={i} arrow>
                <Button appearance='default' block onClick={() => getProgressRoutine(el)}><span className="todayRoutine1">{el}</span></Button>
                </Tooltip>
            )
        })}
        </>
    
    </div>
        }
        
        </>
    )
}

export default ProgressRoutine