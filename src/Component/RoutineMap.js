import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';


const useStyles = makeStyles((theme) => ({
    margin: {
      marginLeft: theme.spacing(1),
      marginTop:theme.spacing(0)
    },
    extendedIcon: {
      marginRight: theme.spacing(1),
    },
  }));

  

const RoutineMap = ({routine, setNewRoutine,setError}) => {
    const classes = useStyles();

    const getRemvedItemFromArray = (el) => {
   
        setNewRoutine(routine.filter(e => e !==el) )
        setError(null)
    }
    return(
        <>
        {routine.map((el,i) => {
            return (
< div className='removeitemdiv' key={i}>
            <TextField id="outlined-basic" label={el} variant="outlined" disabled  />
            <Tooltip title="Delete Routine" arrow>
                    <IconButton onClick={() =>getRemvedItemFromArray(el)}  aria-label="delete" className={classes.margin}>
                            <DeleteIcon  fontSize="small" color="secondary" />
                    </IconButton>
            </Tooltip>
</div>
            )
        })}
        </>
    )
}

export default RoutineMap