import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Tooltip from '@material-ui/core/Tooltip';





const useStyles = makeStyles((theme) => ({
    margin: {
      marginLeft: theme.spacing(115),
      marginTop:theme.spacing(1)
    },
    extendedIcon: {
      marginRight: theme.spacing(1),
    },
  }));

const Nav = ({setIsRoutine,setState,setNewRoutine,setComplete}) => {

    const classes = useStyles();

    const newTask = () => {
        setIsRoutine(false)
        setState([])
        setNewRoutine([])
        setComplete([])

        
          }
    return (
        <div className='Taskdiv' >
        <Tooltip title="Delete Routine" arrow>
        <IconButton onClick={newTask} aria-label="delete" className={classes.margin}>
              <DeleteIcon fontSize="large" />
            </IconButton>
         </Tooltip>
         </div>
    )
}

export default Nav