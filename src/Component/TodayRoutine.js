import React from "react";
import { Button } from 'rsuite';
import Tooltip from '@material-ui/core/Tooltip';


const TodayRoutine = ({routine,setNewRoutine,setState,state}) => {
    const getProgressRoutine = (el) => {

        setNewRoutine(routine.filter(e => e !==el) )
       setState([...state,el])


    }

    return(
        <>
        {routine.length===0?null:
        <div className='card1'>
        <div className='todayRoutine'>Routine for today</div>
        <>
        {routine.map((el,i) => {
            return (
                <Tooltip title="Add to Progress"  key={i} arrow>
                <Button appearance='default' block onClick={() => getProgressRoutine(el)}><span className="todayRoutine1">{el}</span></Button>
                </Tooltip>
            )
        })}
        </>
    
    </div>
        }
        
        </>
    )
}
export default TodayRoutine