import React  from "react";
import RoutineMap from "./RoutineMap";
import SingleRoutine from "./SingleRoutine";


const AddRoutine = ({setIsRoutine,routine,setNewRoutine}) => {
    const [error, setError] = React.useState(null)
   
    const getRoutine = () => {
        if(routine.length<3){
            setError('Add atleast three daily routine')
            return
        } 
       
        setIsRoutine(true)
       
      }

    return (
        <>
       <div className='card'>
        <RoutineMap
         routine={routine}
         setNewRoutine={setNewRoutine} 
         setError={setError} 
          
        />
           
          <SingleRoutine routine={routine} setNewRoutine={setNewRoutine} setError={setError}  />
           {error?<pre className='pre' style={{color:'red'}}>{error}</pre>:null}
           <button className='sendbutton'onClick={getRoutine}>Send</button>
          
           </div>
        </>
    )

}
export default AddRoutine